package com.rave.rickmortyapp.view

sealed class Screens(val route: String) {
    object CharacterScreen : Screens("character")
    object CharacterDetailScreen : Screens("character/{id}")
    object CharacterLocationScreen : Screens("location/{id}")
    object CharacterEpisodeScreen : Screens("episode/{id}")

    fun passId(id: Int): String {
        return this.route.replace(Regex("\\{.*"), "$id")
    }
}
