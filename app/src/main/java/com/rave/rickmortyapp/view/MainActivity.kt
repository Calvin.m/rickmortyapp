package com.rave.rickmortyapp.view

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.rave.rickmortyapp.model.RMRepo
import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.mapper.RMCharacterMapper
import com.rave.rickmortyapp.model.remote.RetrofitClass
import com.rave.rickmortyapp.ui.theme.RickMortyAppTheme
import com.rave.rickmortyapp.view.characterscreen.CharacterScreen
import com.rave.rickmortyapp.view.characterscreen.CharacterScreenState
//import com.rave.rickmortyapp.viewmodel.RMViewModel
//import com.rave.rickmortyapp.viewmodel.VMFactory

class MainActivity : ComponentActivity() {

//    private val rmViewModel by viewModels<RMViewModel> {
//        val repo = RMRepo(
//            RetrofitClass.getRMService(),
//            RMCharacterMapper(),
////            RMCharacterDetailsMapper()
//        )
//        VMFactory(repo)
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        rmViewModel.getCharacters()
        setContent {
//            val rmCharacterState by rmViewModel.characterState.collectAsState()
//            val rmCharacterDetailsState by rmViewModel.characterDetailsState.collectAsState()

            RickMortyAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    SetUpNavGraph()
                }
            }
        }
    }
}
