package com.rave.rickmortyapp.view

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.rave.rickmortyapp.view.characterdetailscreen.CharacterDetailScreen
import com.rave.rickmortyapp.view.characterepisodescreen.CharacterEpisodeScreen
import com.rave.rickmortyapp.view.characterlocationscreen.CharacterLocationScreen
import com.rave.rickmortyapp.view.characterscreen.CharacterScreen
import com.rave.rickmortyapp.viewmodel.RMCharacterLocationViewModel
import com.rave.rickmortyapp.viewmodel.RMCharacterViewModel

@Composable
fun SetUpNavGraph() {
    val navController = rememberNavController()
    val rmCharacterViewModel: RMCharacterViewModel = viewModel()
    val rmCharacterState = rmCharacterViewModel.viewState.collectAsState().value

    NavHost(
        navController = navController, startDestination = Screens.CharacterScreen.route
    ) {
        composable(Screens.CharacterScreen.route) {
            CharacterScreen(
                state = rmCharacterState,
                navigate = { id: Int ->
                    navController.navigate(
                        Screens.CharacterDetailScreen.passId(
                            id
                        )
                    )
                }
            )
        }
        composable(Screens.CharacterDetailScreen.route, arguments = listOf(navArgument("id") {
            type = NavType.IntType
        })) {
            val number = it.arguments?.getInt("id") ?: 0
            CharacterDetailScreen(
                number,
                characterState = rmCharacterState,
                navigate = { screen: String -> navController.navigate(screen) }
            )
        }

        composable(
            Screens.CharacterLocationScreen.route,
            arguments = listOf(navArgument("id") {
                type = NavType.IntType
            })
        ) {
            CharacterLocationScreen(id = it.arguments?.getInt("id") ?: 0)
        }

        composable(
            Screens.CharacterEpisodeScreen.route,
            arguments = listOf(navArgument("id") {
                type = NavType.IntType
            })
        ) {
            CharacterEpisodeScreen(id = it.arguments?.getInt("id") ?: 0)
        }
    }

}

