package com.rave.rickmortyapp.view.characterscreen

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import coil.compose.AsyncImage
import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.view.Screens

@Composable
fun CharacterScreen(
    state: NetworkResponse<List<RMCharacter>>,
    navigate: (Int) -> Unit
) {

    when (state) {
        is NetworkResponse.Error ->
            state.message
        is NetworkResponse.Loading ->
            CircularProgressIndicator()
        is NetworkResponse.Success -> {
            ShowItems(state.data, navigate = navigate)
        }
    }
}

@Composable
fun ShowItems(
    rmCharList: List<RMCharacter>,
    navigate: (Int) -> Unit,
) {
    LazyColumn() {
        items(rmCharList) { char: RMCharacter ->
            RMItem(char, navigate = navigate)
        }
    }

}

@Composable
fun RMItem(
    char: RMCharacter,
    navigate: (Int) -> Unit
) {
    with(char) {
        Row(modifier = Modifier.clickable {
            navigate(char.id)
        }) {
            AsyncImage(
                model = char.image,
                contentDescription = "${char.name} shown as an image."
            )
            Text(text = name)
        }
    }
}
