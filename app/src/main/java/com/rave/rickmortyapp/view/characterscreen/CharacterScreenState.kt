package com.rave.rickmortyapp.view.characterscreen

import com.rave.rickmortyapp.model.local.RMCharacter

data class CharacterScreenState(
    val isLoading: Boolean = false,
    val characters: List<RMCharacter> = emptyList(),
    val error: String = ""
)
