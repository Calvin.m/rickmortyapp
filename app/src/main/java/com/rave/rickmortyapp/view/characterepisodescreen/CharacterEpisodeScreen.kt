package com.rave.rickmortyapp.view.characterepisodescreen

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import com.rave.rickmortyapp.model.local.episode.Episode
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.viewmodel.RMCharacterEpisodeViewModel

@Composable
fun CharacterEpisodeScreen(
    id: Int
) {
    val episodeViewModel: RMCharacterEpisodeViewModel = viewModel()
    val episodeState = episodeViewModel.viewState.collectAsState().value

    episodeViewModel.getEpisode(id)

    when (episodeState) {
        is NetworkResponse.Error -> Text("Error in Character Episode Screen")
        is NetworkResponse.Loading -> CircularProgressIndicator()
        is NetworkResponse.Success -> {
            EpisodeDetails(episodeState.data)
        }
    }
}

@Composable
fun EpisodeDetails(state: Episode) {
    Column() {
        Text(text = state.name)
        Text(text = state.episode)
        Text(text = state.airDate)
        Text(text = state.created)
    }
}

