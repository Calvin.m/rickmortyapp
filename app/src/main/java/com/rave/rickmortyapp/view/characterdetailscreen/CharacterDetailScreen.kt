package com.rave.rickmortyapp.view.characterdetailscreen

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.view.Screens
import com.rave.rickmortyapp.viewmodel.RMCharacterViewModel

@Composable
fun CharacterDetailScreen(
    id: Int,
    characterState: NetworkResponse<List<RMCharacter>>,
    navigate: (String) -> Unit,
) {
    when (characterState) {
        is NetworkResponse.Error -> Text(text = "Error in Character Detail Screen")
        is NetworkResponse.Loading -> CircularProgressIndicator()
        is NetworkResponse.Success -> {
            val selectedCharacter =
                characterState.data.find { id == it.id } ?: characterState.data[0]
            ShowDetails(selectedCharacter, navigate)
        }
    }
}

@Composable
fun ShowDetails(char: RMCharacter, navigate: (String) -> Unit/*, locNavigate: (Int) -> Unit*/) {
    Column() {
        val firstEpisode = char.episode[0].split("/").last()
        Text(text = char.name)
        Text(text = char.location.name, modifier = Modifier.clickable {
            val paths: List<String> = char.location.url.split("/")
            navigate(Screens.CharacterLocationScreen.passId(paths.last().toInt()))
        })
        Text(text = "First Appearance: Episode $firstEpisode", modifier = Modifier.clickable {
            val paths: List<String> = char.episode[0].split("/")
            navigate(Screens.CharacterEpisodeScreen.passId(paths.last().toInt()))
        })
    }
}
