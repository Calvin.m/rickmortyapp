package com.rave.rickmortyapp.view.characterlocationscreen

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.lifecycle.viewmodel.compose.viewModel
import com.rave.rickmortyapp.model.local.RMCharacterLocation
import com.rave.rickmortyapp.model.local.location.Location
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.viewmodel.RMCharacterLocationViewModel

@Composable
fun CharacterLocationScreen(
    id: Int
) {
    val locationViewModel: RMCharacterLocationViewModel = viewModel()
    val locationState = locationViewModel.viewState.collectAsState().value
    locationViewModel.getLoc(id)

    when (locationState) {
        is NetworkResponse.Error -> Row() {}
        is NetworkResponse.Loading -> CircularProgressIndicator()
        is NetworkResponse.Success -> {
            ShowLocation(locationState.data)
        }
    }

}

@Composable
fun ShowLocation(data: Location) {
    Column(
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = "Name: ${data.name}")
        Text(text = "Dimension ${data.dimension}")
        Text(text = "Type: ${data.type}")
//        Text(text = "Residents: ${data.residents}")
        Text(text = "Url: ${data.url}")
    }
}
