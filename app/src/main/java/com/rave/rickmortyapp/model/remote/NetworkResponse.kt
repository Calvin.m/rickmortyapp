package com.rave.rickmortyapp.model.remote

sealed class NetworkResponse<T>(data: T? = null, message: String? = null) {

    data class Success<T>(val data: T) : NetworkResponse<T>(data)

    class Loading<T> : NetworkResponse<T>()

    data class Error<T>(val message: String) : NetworkResponse<T>(null, message)
}
