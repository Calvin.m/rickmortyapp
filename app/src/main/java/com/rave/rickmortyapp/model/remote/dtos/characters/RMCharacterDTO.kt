package com.rave.rickmortyapp.model.remote.dtos.characters

import kotlinx.serialization.Serializable

@Serializable
data class RMCharacterDTO(
    val id: Int = 999,
    val name: String = "",
    val status: String = "",
    val species: String = "",
    val type: String = "",
    val gender: String = "",
    val origin: RMOriginDTO = RMOriginDTO(),
    val location: RMLocationDTO = RMLocationDTO(),
    val image: String = "",
    val episode: List<String> = emptyList(),
    val url: String = "",
    val created: String = ""
)
