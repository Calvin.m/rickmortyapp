package com.rave.rickmortyapp.model.local

data class RMOrigin(
    val name: String,
    val url: String
)
