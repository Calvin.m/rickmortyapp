package com.rave.rickmortyapp.model.mapper.episode

import com.rave.rickmortyapp.model.local.episode.Episode
import com.rave.rickmortyapp.model.mapper.Mapper
import com.rave.rickmortyapp.model.remote.dtos.episode.RMCharacterEpisodeDTO

class EpisodeMapper : Mapper<RMCharacterEpisodeDTO, Episode> {
    override fun invoke(dto: RMCharacterEpisodeDTO): Episode {
        return with(dto) {
            Episode(
                airDate, characters, created, episode, id, name, url
            )
        }
    }
}