package com.rave.rickmortyapp.model.remote.dtos.characters

import com.rave.rickmortyapp.model.local.RMOrigin
import com.rave.rickmortyapp.model.mapper.RMOriginMapper
import kotlinx.serialization.Serializable

@Serializable
data class RMOriginDTO(
    val name: String = "",
    val url: String = ""
) {
    fun toOrigin(mapper: RMOriginMapper): RMOrigin {
        return mapper(this)
    }
}