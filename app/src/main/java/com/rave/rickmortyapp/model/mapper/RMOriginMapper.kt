package com.rave.rickmortyapp.model.mapper

import com.rave.rickmortyapp.model.local.RMOrigin
import com.rave.rickmortyapp.model.remote.dtos.characters.RMOriginDTO

class RMOriginMapper: Mapper<RMOriginDTO, RMOrigin> {
    override fun invoke(dto: RMOriginDTO): RMOrigin = with(dto) {
       RMOrigin(
           name, url
       )
    }
}