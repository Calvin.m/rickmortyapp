package com.rave.rickmortyapp.model.local

data class RMLocation(
    val name: String,
    val url: String
)
