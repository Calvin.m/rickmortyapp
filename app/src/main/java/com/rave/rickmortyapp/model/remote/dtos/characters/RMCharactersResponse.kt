package com.rave.rickmortyapp.model.remote.dtos.characters

import kotlinx.serialization.Serializable

@Serializable
data class RMCharactersResponse(
    val characters: List<RMCharacterDTO> = emptyList()
)
