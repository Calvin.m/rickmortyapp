package com.rave.rickmortyapp.model.local

data class RMCharacterLocation(
    val name: String,
    val url: String
)
