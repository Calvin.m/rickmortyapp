package com.rave.rickmortyapp.model.mapper

import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.remote.dtos.characters.RMCharacterDTO
import com.rave.rickmortyapp.model.remote.dtos.characters.RMLocationDTO

class RMCharacterMapper : Mapper<RMCharacterDTO, RMCharacter> {

    override fun invoke(dto: RMCharacterDTO): RMCharacter {
        return with(dto) {
            RMCharacter(
                id,
                name,
                status,
                species,
                type,
                gender,
                origin = origin.toOrigin(RMOriginMapper()),
                location = location.toLocation(RMLocationMapper()),
                image,
                episode,
                url,
                created
            )
        }
    }
}
