package com.rave.rickmortyapp.model.remote.dtos.characters

import com.rave.rickmortyapp.model.local.RMLocation
import com.rave.rickmortyapp.model.mapper.RMLocationMapper
import kotlinx.serialization.Serializable

@Serializable
data class RMLocationDTO(
    val name: String = "",
    val url: String = ""
) {
    fun toLocation(mapper: RMLocationMapper): RMLocation {
        return mapper(this)
    }
}