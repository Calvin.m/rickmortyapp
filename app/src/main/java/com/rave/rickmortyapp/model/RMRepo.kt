package com.rave.rickmortyapp.model

import android.net.Network
import android.util.Log
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.local.RMCharacterLocation
import com.rave.rickmortyapp.model.local.RMLocation
import com.rave.rickmortyapp.model.local.episode.Episode
import com.rave.rickmortyapp.model.local.location.Location
import com.rave.rickmortyapp.model.mapper.RMCharacterLocationMapper
import com.rave.rickmortyapp.model.mapper.RMCharacterMapper
import com.rave.rickmortyapp.model.mapper.RMLocationMapper
import com.rave.rickmortyapp.model.mapper.episode.EpisodeMapper
import com.rave.rickmortyapp.model.mapper.location.LocationMapper
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.model.remote.RMService
import com.rave.rickmortyapp.model.remote.RetrofitClass
import com.rave.rickmortyapp.model.remote.dtos.characters.RMCharacterDTO
import com.rave.rickmortyapp.model.remote.dtos.characters.RMCharactersResponse
import com.rave.rickmortyapp.model.remote.dtos.characters.RMLocationDTO
import com.rave.rickmortyapp.model.remote.dtos.episode.RMCharacterEpisodeDTO
import com.rave.rickmortyapp.model.remote.dtos.location.RMCharacterLocationDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

object RMRepo {
    private val service: RMService by lazy { RetrofitClass.getRMService() }
    private val mapper: RMCharacterMapper = RMCharacterMapper()
    private val locMapper: LocationMapper = LocationMapper()
    private val epMapper: EpisodeMapper = EpisodeMapper()

    suspend fun getRMCharacters(ids: List<Int>): NetworkResponse<List<RMCharacter>> =
        withContext(Dispatchers.IO) {
            val serviceResponse: Response<List<RMCharacterDTO>> =
                service.getRMCharacters(ids.joinToString(",")).execute()

            return@withContext if (serviceResponse.isSuccessful) {
                val characterResponse = serviceResponse.body() ?: listOf(RMCharacterDTO())
//                val characterResponse = networkResponse.body() ?: listOf(RMCharacter())
                val characterList = characterResponse.map { mapper(it) }
                NetworkResponse.Success(characterList)
            } else {
                NetworkResponse.Error(serviceResponse.message())
            }
        }

//    suspend fun getRMCharacterDetails(id: Int): NetworkResponse<RMCharacter> =
//        withContext(Dispatchers.IO) {
//            val serviceResponse: Response<RMCharacterDTO> =
//                service.getRMCharacterDetails(id).execute()
//            return@withContext if (serviceResponse.isSuccessful) {
//                // missing comp obj - needs initializing
//                val characterResponse = serviceResponse.body() ?: RMCharacterDTO()
////                val characterResponse = networkResponse.body() ?: listOf(RMCharacter())
//                val returnedCharResponse = mapper(characterResponse)
//                NetworkResponse.Success(returnedCharResponse)
//            } else {
//                NetworkResponse.Error(serviceResponse.message())
//            }
//        }

    suspend fun getLocation(id: Int): NetworkResponse<Location> = withContext(Dispatchers.IO) {

        val serviceResponse: Response<RMCharacterLocationDTO> =
            service.getRMCharacterLocation(id).execute()

        return@withContext if (serviceResponse.isSuccessful) {
            val locationResponse = serviceResponse.body() ?: RMCharacterLocationDTO()
            val returnedLocResponse = locMapper(locationResponse)
            NetworkResponse.Success(returnedLocResponse)
        } else {
            NetworkResponse.Error(serviceResponse.message())
        }
    }

    suspend fun getEpisode(id: Int): NetworkResponse<Episode> = withContext(Dispatchers.IO) {
        val serviceResponse: Response<RMCharacterEpisodeDTO> =
            service.getRMCharacterEpisode(id).execute()

        return@withContext if (serviceResponse.isSuccessful) {
            val episodeResponse = serviceResponse.body() ?: RMCharacterEpisodeDTO()
            val returnedEpisodeResponse = epMapper(episodeResponse)
            NetworkResponse.Success(returnedEpisodeResponse)
        } else {
            NetworkResponse.Error(serviceResponse.message())
        }
    }
}
