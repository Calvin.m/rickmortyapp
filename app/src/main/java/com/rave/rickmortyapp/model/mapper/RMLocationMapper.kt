package com.rave.rickmortyapp.model.mapper

import com.rave.rickmortyapp.model.local.RMLocation
import com.rave.rickmortyapp.model.remote.dtos.characters.RMLocationDTO

class RMLocationMapper : Mapper<RMLocationDTO, RMLocation> {
    override fun invoke(dto: RMLocationDTO): RMLocation = with(dto) {
        RMLocation(
            name, url
        )
    }
}