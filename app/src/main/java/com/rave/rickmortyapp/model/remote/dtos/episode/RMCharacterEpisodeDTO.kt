package com.rave.rickmortyapp.model.remote.dtos.episode

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class RMCharacterEpisodeDTO(
    @SerialName("air_date")
    val airDate: String = "",
    val characters: List<String> = emptyList(),
    val created: String = "",
    val episode: String = "",
    val id: Int = 0,
    val name: String = "",
    val url: String = ""
)