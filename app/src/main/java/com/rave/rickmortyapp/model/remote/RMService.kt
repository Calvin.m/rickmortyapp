package com.rave.rickmortyapp.model.remote

import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.remote.dtos.characters.RMCharacterDTO
import com.rave.rickmortyapp.model.remote.dtos.characters.RMCharactersResponse
import com.rave.rickmortyapp.model.remote.dtos.episode.RMCharacterEpisodeDTO
import com.rave.rickmortyapp.model.remote.dtos.location.RMCharacterLocationDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RMService {
    @GET(RM_CHARACTERS_ENDPOINT + IDS_QUERY)
    fun getRMCharacters(@Path("ids") ids: String): Call<List<RMCharacterDTO>>

    @GET(RM_CHARACTER_LOCATION_ENDPOINT + ID_QUERY)
    fun getRMCharacterLocation(@Path("id") id: Int): Call<RMCharacterLocationDTO>

    @GET(RM_CHARACTER_EPISODE_ENDPOINT + ID_QUERY)
    fun getRMCharacterEpisode(@Path("id") id: Int): Call<RMCharacterEpisodeDTO>

    companion object {
        private const val RM_CHARACTERS_ENDPOINT = "character/"
        private const val RM_CHARACTER_LOCATION_ENDPOINT = "location/"
        private const val RM_CHARACTER_EPISODE_ENDPOINT = "episode/"
        private const val IDS_QUERY = "{ids}"
        private const val ID_QUERY = "{id}"
    }
}
