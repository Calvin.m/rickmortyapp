package com.rave.rickmortyapp.model.mapper.location

import com.rave.rickmortyapp.model.local.location.Location
import com.rave.rickmortyapp.model.mapper.Mapper
import com.rave.rickmortyapp.model.remote.dtos.characters.RMLocationDTO
import com.rave.rickmortyapp.model.remote.dtos.location.RMCharacterLocationDTO

class LocationMapper : Mapper<RMCharacterLocationDTO, Location> {
    override fun invoke(dto: RMCharacterLocationDTO): Location {
        return with(dto) {
            Location(
                created,
                dimension,
                id,
                name,
                residents,
                type,
                url
            )
        }
    }
}