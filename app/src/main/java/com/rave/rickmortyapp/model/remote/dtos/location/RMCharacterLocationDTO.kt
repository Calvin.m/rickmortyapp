package com.rave.rickmortyapp.model.remote.dtos.location

import kotlinx.serialization.Serializable

@Serializable
data class RMCharacterLocationDTO(
    val created: String = "",
    val dimension: String = "",
    val id: Int = 0,
    val name: String = "",
    val residents: List<String> = emptyList(),
    val type: String = "",
    val url: String = ""
)