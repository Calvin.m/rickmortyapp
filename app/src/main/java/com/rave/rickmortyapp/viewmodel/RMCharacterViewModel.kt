package com.rave.rickmortyapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.rickmortyapp.model.RMRepo
import com.rave.rickmortyapp.model.local.RMCharacter
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.view.characterscreen.CharacterScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class RMCharacterViewModel: ViewModel() {
    private val repo = RMRepo

    private val _viewState: MutableStateFlow<NetworkResponse<List<RMCharacter>>> =
        MutableStateFlow(NetworkResponse.Loading())
    val viewState: StateFlow<NetworkResponse<List<RMCharacter>>>
        get() = _viewState

    init {
        getCharacters()
    }

    private fun getCharacters() = viewModelScope.launch {
        _viewState.value = repo.getRMCharacters((1..15).toList())
    }
}
