package com.rave.rickmortyapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.rickmortyapp.model.RMRepo
import com.rave.rickmortyapp.model.local.episode.Episode
import com.rave.rickmortyapp.model.remote.NetworkResponse
import com.rave.rickmortyapp.model.remote.dtos.episode.RMCharacterEpisodeDTO
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class RMCharacterEpisodeViewModel : ViewModel() {
    private val repo = RMRepo

    private val _viewState: MutableStateFlow<NetworkResponse<Episode>> =
        MutableStateFlow(NetworkResponse.Loading())
    val viewState: StateFlow<NetworkResponse<Episode>> get() = _viewState

    fun getEpisode(id: Int) = viewModelScope.launch {
        val answer = repo.getEpisode(id)
        _viewState.value = answer
    }
}