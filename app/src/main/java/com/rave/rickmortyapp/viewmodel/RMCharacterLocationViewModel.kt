package com.rave.rickmortyapp.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rave.rickmortyapp.model.RMRepo
import com.rave.rickmortyapp.model.local.RMCharacterLocation
import com.rave.rickmortyapp.model.local.RMLocation
import com.rave.rickmortyapp.model.local.location.Location
import com.rave.rickmortyapp.model.remote.NetworkResponse
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class RMCharacterLocationViewModel: ViewModel() {
    private val repo = RMRepo

    private val _viewState: MutableStateFlow<NetworkResponse<Location>> =
        MutableStateFlow(NetworkResponse.Loading())
    val viewState: StateFlow<NetworkResponse<Location>> get() = _viewState

    fun getLoc(id: Int) = viewModelScope.launch {
        _viewState.value = repo.getLocation(id)
    }
}
